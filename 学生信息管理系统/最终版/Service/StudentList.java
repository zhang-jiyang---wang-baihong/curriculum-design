/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import java.util.ArrayList;

/**
 *
 * @author 86159
 */
public class StudentList {                                        //学生列表类，用于存储学生列表，并对学生列表进行操作

    private ArrayList<Student> students;                            //存储Student的ArrayList

    public StudentList(ArrayList<Student> students) {            //构造函数
        this.students = students;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    StudentList() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Boolean add(Student student) {                       //在列表里添加一个学生，如果添加学生的学号已经存在，则取消添加并返回false，添加成功则返回true
        if(SearchID(student.getId())!=-1){
            return false;
        }
        students.add(student);
        return true;
    }
    
    public Boolean add(String id, String name, String gender, String birthDay,  String polOutlook, String address, String tel, String dor) {//在列表里添加一个学生，如果添加学生的学号已经存在，则取消添加并返回false，添加成功则返回true
        Student student=new Student(id,name, birthDay, gender, polOutlook, address, tel, dor);
        return students.add(student);
    }
    
    public Boolean remove(Student student){                        //在列表中移除一个学生
        return students.remove(student);
    }
     
    public Boolean delete(int[] index){                           //删除下标数组的所有学生
        for(int i=index.length-1;i>=0;i--){
            students.remove(index[i]);
        }
       
        return true;
    }
    
    
    public int SearchName(String Name){                          //按姓名在列表中搜索学生，返回学生对应的下标，没找到返回-1
        int index = -1;
        
        for(Student student:students){
            if(student.getName().equals(Name)){
                index = students.indexOf(student);
            }
        }
     
        return index;
    }
    
    public int SearchID(String Id){                             //按学号在列表中搜索学生，返回学生对应的下标，没找到返回-1
        int index = -1;
        
        for(Student student:students){
            if(student.getId().equals(Id)){
                index = students.indexOf(student);
            } 
        }
     
        return index;
    }

    @Override
    public String toString() {                                //toString输出学生列表，一行一个学生信息
        String outPut = "";
        for(Student stu:students){
            outPut=outPut+stu.toString()+"\n";
        }
        
        return outPut;
    }

    
    public Student getStudent(int index) {                    //按下标返回一个Student
        return students.get(index);
    }
    
    
     public Student changeStudent(int index,Student stu){       //将学生列表内对应下标的学生改为传进的Student
        return students.set(index, stu);
    }
     
}