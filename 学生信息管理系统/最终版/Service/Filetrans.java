/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author 86159
 */
    public class Filetrans {
        
    private File file;
    
    public Filetrans(File file){
        this.file=file;
    }
    
    public ArrayList<Student> read(){
//将该文件中的数据进行处理，将每行数据保存到一个Student中，再将这些Student保存到一个ArrayList students中，最后返回这个students
        ArrayList<Student> students = new ArrayList<>();
        try { 
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis,"utf-8");
            BufferedReader br = new  BufferedReader(isr);
            String str =null;
            while((str=br.readLine())!=null){//逐行读取文件数据
                String []arrays =str.split(" ");//以空格为间隔
                Student stu = new Student(arrays[0],arrays[1], arrays[2], arrays[3], arrays[4], arrays[5], arrays[6], arrays[7]);
                students.add(stu);//将一个student加入到students中
            }
            br.close();
            fis.close();
        } catch (IOException e) {
            System.out.println("读入失败");
        }
        return students;
    }
    
}
