/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

/**
 *
 * @author 86159
 */


public class Student {                                                         //学生类，用于存储学生信息
private String id;                                                              //学号
private String name;                                                            //姓名
private String birthDay;                                                        //生日
private String gender;                                                          //性别
private String polOutlook;                                                      //政治面貌
private String address;                                                         //家庭住址
private String tel;                                                             //电话
private String dor;                                                             //宿舍号

    public Student(String id, String name, String gender,String birthDay,  String polOutlook, String address, String tel, String dor) {//构造函数
        this.id = id;
        this.name = name;
        this.birthDay = birthDay;
        this.gender = gender;
        this.polOutlook = polOutlook;
        this.address = address;
        this.tel = tel;
        this.dor = dor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPolOutlook() {
        return polOutlook;
    }

    public void setPolOutlook(String polOutlook) {
        this.polOutlook = polOutlook;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDor() {
        return dor;
    }

    public void setDor(String dor) {
        this.dor = dor;
    }

    @Override
    public String toString() {
        return id+"  "+name+"  "+gender+"  "+birthDay+"  "+polOutlook+"  "+address+"  "+tel+"  "+dor;
        
    }

    public Boolean IsLegal(){                                                  //判断学生数据是否合法，所有数据均不能为空
        
        if(id==null||name==null||gender==null||birthDay==null||polOutlook==null||address==null||tel==null||dor==null){
            return false;
        }
        
        if(id.length()>0&&name.length()>0&&gender.length()>0&&birthDay.length()>0&&polOutlook.length()>0&&address.length()>0&&tel.length()>0&&dor.length()>0){
            return true;
        }else
        {
            return false;
        }
    }
  
    

    
}