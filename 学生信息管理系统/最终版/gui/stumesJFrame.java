/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import Service.Filetrans;
import Service.Student;
import Service.StudentList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;



/**
 *
 * @author 86159
 */
public class stumesJFrame extends javax.swing.JFrame {

    /**
     * Creates new form stumesJFrame
     */
     static ArrayList<Student> students = new ArrayList<>();//定义一个存放学生的数组
     static StudentList studentList = new StudentList(students);//将这个数组放到StudentList类中
    public stumesJFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtFileChooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        importButton = new javax.swing.JButton();
        searchButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        modifyButton = new javax.swing.JButton();
        outputButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane1.setName(""); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "学号", "姓名", "性别", "出生日期", "政治面貌", "家庭住址", "电话", "宿舍号"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setResizable(false);
            jTable1.getColumnModel().getColumn(5).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(6).setResizable(false);
            jTable1.getColumnModel().getColumn(6).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(7).setResizable(false);
            jTable1.getColumnModel().getColumn(7).setPreferredWidth(50);
        }

        importButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        importButton.setText("导入学生信息");
        importButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButtonActionPerformed(evt);
            }
        });

        searchButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        searchButton.setText("查找");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        addButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        addButton.setText("增加");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        deleteButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        deleteButton.setText("删除");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        modifyButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        modifyButton.setText("修改");
        modifyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifyButtonActionPerformed(evt);
            }
        });

        outputButton.setFont(new java.awt.Font("宋体", 0, 24)); // NOI18N
        outputButton.setText("导出学生信息");
        outputButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(importButton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(outputButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(modifyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(79, 79, 79))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(importButton, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(outputButton, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                    .addComponent(deleteButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(modifyButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed

      int[] selectedRows =jTable1.getSelectedRows();//用数组来存放选中的一行或多行
      DefaultTableModel model =(DefaultTableModel)jTable1.getModel();
      for(int i=selectedRows.length;i>0;i--){
          model.removeRow(jTable1.getSelectedRow());//移除表格中选中的行（外部）
      }
        studentList.delete(selectedRows);//在studentList中删除选中的行（内部）
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void importButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importButtonActionPerformed
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Txt&CSV","txt","csv");
        txtFileChooser.setFileFilter(filter);//文件选择框
        int val =txtFileChooser.showOpenDialog(this);
        if(val==txtFileChooser.APPROVE_OPTION){
            File selectedFile =txtFileChooser.getSelectedFile();//selectedFile为选中的文件
            Filetrans  txtfile =new Filetrans(selectedFile);//将选中的文件存到Filetrans类中
            students=txtfile.read();//调用Filetrans类中的read方法，得到一个学生列表students
            studentList = new StudentList(students);//将得到的students保存在StudentList类中
            DefaultTableModel model =(DefaultTableModel)jTable1.getModel();
            for (Student e : students) {
                //获取每个学生的信息
                String id =e.getId();
                String name=e.getName();
                String birthday=e.getBirthDay();
                String gender=e.getGender();
                String PolOutlook=e.getPolOutlook();
                String address =e.getAddress();
                String tel =e.getTel();
                String dor =e.getDor();
                Object[] o ={id,name,gender,birthday,PolOutlook,address,tel,dor};
                model.addRow(o);//添加一行，该行存放一个学生的信息
            }
        }
        
    }//GEN-LAST:event_importButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        AddDialog  addDialog = new AddDialog(this, true);
        addDialog.setVisible(true);//打开addDialog界面
        DefaultTableModel model =(DefaultTableModel)jTable1.getModel();
        if(studentList.SearchID(addDialog.stu.getId())==-1&&addDialog.stu.IsLegal()){//如果要添加的学号原本并不存在，且要添加的学生信息合法
            stumesJFrame.studentList.add(addDialog.stu);//在studentList中添加该学生（内部）
            Object[] o ={addDialog.stu.getId(),addDialog.stu.getName(),addDialog.stu.getGender(),addDialog.stu.getBirthDay(),addDialog.stu.getPolOutlook(),addDialog.stu.getAddress(),addDialog.stu.getTel(),addDialog.stu.getDor()};
            model.addRow(o);//在表格中添加一行该学生的信息（外部）
        }  
    }//GEN-LAST:event_addButtonActionPerformed

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
       SearchDialog searchDialog = new SearchDialog(this,true);
       searchDialog.setVisible(true);//打开searchDialog界面
       
    }//GEN-LAST:event_searchButtonActionPerformed

    private void modifyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifyButtonActionPerformed
      
        ModifyDialog modifyDialog = new ModifyDialog(this, true);
        modifyDialog.setVisible(true);//打开修改界面
        
        if(ModifyDialog.newstu.IsLegal()){//如果要修改的学生信息填写规范（合法）
            //将ModifyDialong得到的一个修改完的新的学生newstu的信息，在表格中的相应位置进行更改（外部）
            DefaultTableModel model =(DefaultTableModel)jTable1.getModel();
            model.setValueAt(ModifyDialog.newstu.getName(), ModifyDialog.index, 1);
            model.setValueAt(ModifyDialog.newstu.getGender(), ModifyDialog.index, 2);
            model.setValueAt(ModifyDialog.newstu.getBirthDay(), ModifyDialog.index, 3);
            model.setValueAt(ModifyDialog.newstu.getPolOutlook(), ModifyDialog.index,4);
            model.setValueAt(ModifyDialog.newstu.getAddress(), ModifyDialog.index, 5);
            model.setValueAt(ModifyDialog.newstu.getTel(), ModifyDialog.index, 6);
            model.setValueAt(ModifyDialog.newstu.getDor(), ModifyDialog.index, 7);
        }
       
        
    }//GEN-LAST:event_modifyButtonActionPerformed

    private void outputButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputButtonActionPerformed
        File file = new File("学生信息.txt");//新建一个文件
        
        try{
            FileWriter fw = new FileWriter(file);
            for (Student e : studentList.getStudents()) {
                fw.write(e.toString()+'\n');//逐行写入每个学生的数据
                fw.flush();
            }
             fw.close();
           
        }catch(IOException e){
            e.printStackTrace();
        }
        
       JOptionPane.showMessageDialog(null, "导出成功", "",JOptionPane.INFORMATION_MESSAGE); 
    }//GEN-LAST:event_outputButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(stumesJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(stumesJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(stumesJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(stumesJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new stumesJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton importButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton modifyButton;
    private javax.swing.JButton outputButton;
    private javax.swing.JButton searchButton;
    private javax.swing.JFileChooser txtFileChooser;
    // End of variables declaration//GEN-END:variables
}
